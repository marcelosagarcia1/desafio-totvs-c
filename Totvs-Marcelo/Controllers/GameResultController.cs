﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TotvsMarcelo.Models;

namespace TotvsMarcelo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameResultController : ControllerBase
    {
        //Propriedade que vai instanciar o contexto do banco de dados
        private readonly GameResultContext _context;


        public GameResultController(GameResultContext context)
        {
            //Aqui o construtor está utilizando injecao de dependencia para injetar o contexto da nossa classe context
            _context = context;
            //Cria o banco caso ele nao exista
            _context.Database.EnsureCreated();
        }

        #region Metodos para a api

        [HttpGet]
        public async Task<ActionResult> GetPlayers()
        {
            var dataPlayers = await Task.Run(() => GetTop100Players());
            return Ok(dataPlayers);   
        }


        [HttpGet("{list}", Name = "GetResults")]
        public async Task <ActionResult<GameResultCollection>> GetResultsById(List<GameResult> gamesList)
        {
            return await Task.Run(()=> GetCreatedPlayers(gamesList));
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] GameResultCollection result)
        {
            return await Task.Run(() => CreateResult(result));
        }

        #endregion

        #region Private Methods
        //poderia otimizar esse método
        private IActionResult CreateResult(GameResultCollection result)
        {
            var gamesList = new List<GameResult>();

            result.GameResults.ToList().ForEach(r =>
            {
                var resultPlayerInDB = _context.Players.Find(r.PlayerId);
                var resultGameInDB = _context.Games.Find(r.GameId);

                if (resultPlayerInDB != null)
                {
                    resultPlayerInDB.Win += r.Win;
                    _context.Players.Update(resultPlayerInDB);
                }
                else
                    _context.Players.Add(new Player { PlayerId = r.PlayerId, Win = r.Win, GameId = r.GameId});

                if (resultGameInDB != null)
                {
                    resultGameInDB.Timestamp = r.Timestamp;
                    _context.Games.Update(resultGameInDB);
                }
                else
                    _context.Games.Add(new Game { GameId = r.GameId, Timestamp = r.Timestamp });
               
                gamesList.Add(new GameResult { PlayerId = r.PlayerId, GameId = r.GameId, Win = r.Win, Timestamp = r.Timestamp });

                _context.SaveChanges();
            });

            return CreatedAtRoute("GetResults", new { list = gamesList }, result);
        }

        private List<Player> GetTop100Players()
        {
            var playerScore = (from p in _context.Players
                               orderby p.Win descending
                               select p).Take(100);

            return playerScore.ToList();
        }

         
        private ActionResult<GameResultCollection> GetCreatedPlayers(List<GameResult> gameResult)
        {
            var itemsSaved = new GameResultCollection();

            gameResult.ToList().ForEach(i =>
            {
                //apenas para validar que as informaçoes estao realmente vindo do banco de dados e nao por parametro
                var player = _context.Players.Find(i.PlayerId);
                var game = _context.Games.Find(i.GameId);

                itemsSaved.GameResults.Add(new GameResult{GameId= game.GameId,PlayerId= player.PlayerId,Win=player.Win,Timestamp=game.Timestamp});
            });

            if (itemsSaved == null)
            {
                return NotFound();
            }

            return itemsSaved;   
        }

        #endregion
    }
}
