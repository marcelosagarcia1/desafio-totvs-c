﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TotvsMarcelo.Models
{
    //Model para receber os objetos do tipo GameResult
    public class GameResult
    {
        public int PlayerId { get; set; }
        public int GameId { get; set; }
        public long Win { get; set; }
        public string Timestamp { get; set; }
    }
}
