﻿using System;
using Microsoft.EntityFrameworkCore;

namespace TotvsMarcelo.Models
{
    public class GameResultContext: DbContext
    {
        // Esta é a classe que comanda todo o contexto e operaçoes do entity framework no projeto.
        // Estou utilizando o EntityFramework pois eu vejo como o mais simples e de melhor manutençao para este projeto.
        public GameResultContext(DbContextOptions<GameResultContext> options)
            :base(options)
        {
        }

        public DbSet<Player> Players { get; set; }
        public DbSet<Game> Games { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>(entity =>
            {
                entity.HasKey(e => e.PlayerId);
                entity.Property(e => e.Win);
                entity.Property(e => e.GameId);
            });

            modelBuilder.Entity<Game>(entity =>
            {
                entity.HasKey(e => e.GameId);
                entity.Property(e => e.Timestamp);
                entity.HasOne(d => d.Player)
                  .WithMany(p => p.Games);
            });
           
        }
    }
}
