﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TotvsMarcelo.Models
{
    public class Player
    {
        [Key]
        public int PlayerId { get; set; }
        public int GameId { get; set; }
        public long Win { get; set; }

        //Para o caso de um futuro proximo precisarmos listar os jogos.
        [JsonIgnore]
        public virtual ICollection<Game> Games { get; set; }
    }
}
