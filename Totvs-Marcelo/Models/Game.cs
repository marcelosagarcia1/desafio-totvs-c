﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TotvsMarcelo.Models
{
    public class Game
    {
        [Key]
        public int GameId { get; set; }
        public string Timestamp { get; set; }
        public virtual Player Player { get; set; }
    }

    public class GameResultCollection
    {
        [JsonProperty("gameResults")]
        public ICollection<GameResult> GameResults { get; set; }
    }
}
